# -*- coding: utf-8 -*-
from django.contrib import admin

from .models import Wallet, Transaction


@admin.register(Wallet)
class WalletAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'owned_by',
        'status',
        'enabled_at',
        'created_at',
        'updated_at',
        'deleted_at',
    )
    list_filter = (
        'owned_by',
        'enabled_at',
        'created_at',
        'updated_at',
        'deleted_at',
    )
    date_hierarchy = 'created_at'


@admin.register(Transaction)
class TransactionAdmin(admin.ModelAdmin):
    list_display = (
        'status',
        'id',
        'wallet',
        'amount',
        'trn_type',
        'trn_by',
        'trn_at',
        'created_at',
        'updated_at',
    )
    list_filter = ('wallet', 'trn_at', 'created_at', 'updated_at')
    date_hierarchy = 'created_at'
