from django.utils.decorators import classonlymethod


class StatusChoice:
    SUCCESS = 'S'
    FAIL = 'F'

    @classonlymethod
    def get_choices(cls):
        return (
            (cls.SUCCESS, 'Success'),
            (cls.FAIL, 'Fail')
        )


STATUS_CHOICE_LIST = StatusChoice.get_choices()


class TransactionType:
    CREDIT = 'C'
    DEBIT = 'D'

    @classonlymethod
    def get_choices(cls):
        return (
            (cls.CREDIT, 'Credit'),
            (cls.DEBIT, 'Debit')
        )


TRANSACTION_TYPE_LIST = TransactionType.get_choices()


class WalletStatus:
    ENABLE = 'E'
    DISABLE = 'D'

    @classonlymethod
    def get_choices(cls):
        return (
            (cls.ENABLE, 'Enable'),
            (cls.DISABLE, 'Disable')
        )


WALLET_STATUS_CHOICE = WalletStatus.get_choices()
