# Generated by Django 3.2.9 on 2021-11-09 16:47

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mini_wallet', '0004_auto_20211109_1322'),
    ]

    operations = [
        migrations.RenameField(
            model_name='transaction',
            old_name='deposit_balance',
            new_name='amount',
        ),
        migrations.RenameField(
            model_name='transaction',
            old_name='deposited_at',
            new_name='trn_at',
        ),
        migrations.RenameField(
            model_name='transaction',
            old_name='deposited_by',
            new_name='trn_by',
        ),
        migrations.RemoveField(
            model_name='transaction',
            name='reference_id',
        ),
        migrations.RemoveField(
            model_name='transaction',
            name='withdraw_balance',
        ),
        migrations.RemoveField(
            model_name='transaction',
            name='withdrawn_at',
        ),
        migrations.RemoveField(
            model_name='transaction',
            name='withdrawn_by',
        ),
        migrations.AddField(
            model_name='transaction',
            name='trn_type',
            field=models.CharField(choices=[('Credit', 'C'), ('Debit', 'D')], default=None, max_length=7),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='wallet',
            name='deleted_at',
            field=models.DateTimeField(editable=False, null=True),
        ),
    ]
