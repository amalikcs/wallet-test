# Generated by Django 3.2.9 on 2021-11-09 18:34

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mini_wallet', '0008_auto_20211109_1833'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='wallet',
            options={'ordering': ['-created_at'], 'verbose_name': 'wallet', 'verbose_name_plural': 'wallets'},
        ),
    ]
