import uuid

from django.conf import settings
from django.db import models
from .choices import (STATUS_CHOICE_LIST,
                      TRANSACTION_TYPE_LIST,
                      TransactionType,
                      WALLET_STATUS_CHOICE, WalletStatus)
from django.db.models import Sum
from decimal import Decimal


class Wallet(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4)
    owned_by = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='wallet')
    status = models.CharField(max_length=7, choices=WALLET_STATUS_CHOICE, default=WalletStatus.ENABLE)
    enabled_at = models.DateTimeField(auto_now_add=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    deleted_at = models.DateTimeField(editable=False, null=True)

    class Meta:
        verbose_name = 'wallet'
        verbose_name_plural = 'wallets'
        ordering = ['-created_at']

    def __str__(self):
        return str(self.id)

    @property
    def balance(self):
        qs = Transaction.objects.filter(wallet=self)
        credit_balance = qs.filter(trn_type=TransactionType.CREDIT).aggregate(Sum('amount'))['amount__sum'] or Decimal(0)
        debt_balance = qs.filter(trn_type=TransactionType.DEBIT).aggregate(Sum('amount'))['amount__sum'] or Decimal(0)
        print(credit_balance, debt_balance)
        return credit_balance - debt_balance

    def add_amount(self, amount):
        Transaction.objects.create(wallet=self, amount=amount, trn_type=TransactionType.CREDIT)

    def withdraw_amount(self, amount):
        if self.balance > amount:
            Transaction.objects.create(wallet=self, amount=amount, trn_type=TransactionType.DEBIT)
        else:
            raise Exception("insufficient Amount")


class Transaction(models.Model):

    status = models.CharField(max_length=7, choices=STATUS_CHOICE_LIST)
    id = models.UUIDField(primary_key=True, default=uuid.uuid4)
    wallet = models.ForeignKey(Wallet, on_delete=models.CASCADE, related_name='transaction')
    amount = models.DecimalField(max_digits=6, decimal_places=2)
    trn_type = models.CharField(max_length=7, choices=TRANSACTION_TYPE_LIST, default=None)
    trn_by = models.UUIDField(default=uuid.uuid4)
    trn_at = models.DateTimeField(auto_now_add=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = 'transaction'
        verbose_name_plural = 'transactions'
        ordering = ['-created_at']

