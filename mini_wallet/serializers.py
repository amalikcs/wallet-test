from rest_framework import serializers
from django.contrib.auth.models import User
from .models import Wallet, Transaction
from django.urls import reverse
from rest_framework.authtoken.models import Token


class UserCreateSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ['username']


    def to_representation(self, instance):
        token_data = {}
        status = ''
        data = {
            'data': token_data,
            'status': status
        }
        token, created = Token.objects.get_or_create(user=instance)
        if created:
            token_data.update({'token': token.key})
            data.update({'status': 'success'})
        return data


class UserSerializer(serializers.ModelSerializer):

    class Meta:

        model = User
        fields = ['id', 'username', 'is_staff', 'is_superuser', 'first_name', 'last_name']

    def to_representation(self, instance):
        data = super(UserSerializer, self).to_representation(instance)
        data['userDetailUrl'] = reverse('api:user_detail', args=[instance.pk])
        data['ToggleURL'] = reverse('api:enable_wallet', args=[instance.pk])
        return data


class WalletSerializer(serializers.ModelSerializer):

    class Meta:
        model = Wallet
        # fields = '__all__'
        exclude = ['created_at', 'updated_at', 'deleted_at']

    def to_representation(self, instance):
        wallet = super(WalletSerializer, self).to_representation(instance)
        wallet.update({'balance': instance.balance})
        wallet.update({'status': instance.get_status_display()})
        response = {
            'status': 'success',
            "data": wallet
        }
        return response


class TransactionSerializer(serializers.ModelSerializer):

    class Meta:
        model = Transaction
        exclude = ['created_at', 'updated_at', 'status', 'wallet', 'trn_type']

    def to_representation(self, instance):
        transaction = super(TransactionSerializer, self).to_representation(instance)
        transaction.update({'status': instance.get_status_display()})
        transaction.update({'reference_id': instance.id})
        transaction['deposited_by'] = transaction['trn_by']
        transaction['deposited_at'] = transaction['trn_at']
        del transaction['trn_at'], transaction['trn_by']
        response = {
            'status': 'success',
            'data': transaction
        }
        return response

