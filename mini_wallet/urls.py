from django.urls import re_path, path

from .views import (UserCreateAPIView,
                    UserDetailAPIView,
                    EnableWalletAPIView,
                    DepositWalletAPIView)


urlpatterns = [
    path('init/', UserCreateAPIView.as_view(), name="user_list"),
    path('wallet/<int:pk>/', UserDetailAPIView.as_view(), name="user_detail"),
    path('wallet/', EnableWalletAPIView.as_view(), name="enable_wallet"),
    path('wallet/deposit/', DepositWalletAPIView.as_view(), name="deposit"),
]
