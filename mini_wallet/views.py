
from rest_framework.generics import ListAPIView, RetrieveAPIView, CreateAPIView, UpdateAPIView
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK, HTTP_201_CREATED, HTTP_400_BAD_REQUEST
from .serializers import UserSerializer, WalletSerializer, TransactionSerializer, UserCreateSerializer
from rest_framework.authentication import TokenAuthentication, SessionAuthentication
from django.contrib.auth import get_user_model
from .models import Wallet, Transaction
from .choices import WalletStatus, TransactionType, StatusChoice
import uuid
User = get_user_model()


class UserCreateAPIView(CreateAPIView):
    serializer_class = UserCreateSerializer
    queryset = User.objects.all()
    authentication_classes = []
    permission_classes = []


class UserDetailAPIView(RetrieveAPIView):
    serializer_class = UserSerializer
    queryset = User.objects.all()
    authentication_classes = []
    permission_classes = []


class EnableWalletAPIView(APIView):
    authentication_classes = [TokenAuthentication, SessionAuthentication]

    def post(self, request, *args, **kwargs):
        w, created = Wallet.objects.get_or_create(owned_by=request.user)
        w.status = WalletStatus.ENABLE
        w.save()
        serializer = WalletSerializer(w)
        return Response(serializer.data, status=HTTP_200_OK)


class DepositWalletAPIView(APIView):
    authentication_classes = [TokenAuthentication, SessionAuthentication]

    def post(self, request, *args, **kwargs):
        wallet = request.user.wallet
        serializer = TransactionSerializer(data=request.data)
        if serializer.is_valid():
            amount = serializer.validated_data['amount']
            trx = Transaction.objects.create(wallet=wallet,
                                            amount=amount,
                                            trn_type=TransactionType.CREDIT,
                                            trn_by=uuid.uuid4(),
                                            status=StatusChoice.SUCCESS)
            serializer = TransactionSerializer(trx)
            return Response(serializer.data, status=HTTP_201_CREATED)
        return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)


